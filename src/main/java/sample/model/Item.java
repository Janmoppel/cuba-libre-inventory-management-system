package sample.model;

import sun.text.resources.FormatData;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Item {
    private String title;
    private int quantity;
    private Date expirationDate;

    public Item() {
    }

    public Item(String title, String quantity, String expirationDate) throws Exception {
        setTitle(title);
        setQuantity(quantity);
        setExpirationDate(expirationDate);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if(title.length() > 50)
            title = title.substring(0, 50);
        this.title = title;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(String quantity) throws Exception {
        try {
            int quantityInt = Integer.parseInt(quantity);
            if (quantityInt < 0)
                throw new Exception("Quantity has to be positive");
            this.quantity = quantityInt;
        } catch (NumberFormatException nfe) {
            throw new Exception("Quantity has to be integer");
        }
    }

    public Date getExpirationDate() {
        return this.expirationDate;
    }

    public void setExpirationDate(String expirationDate) throws Exception {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = formatter.parse(expirationDate);
            if(date.before(new Date()))
                throw new Exception("Expiration date cannot be set for past dates");
            this.expirationDate = date;
        } catch (ParseException pe) {
            throw new Exception("Wrong format for expiration date");
        }
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return title + ", " + quantity + ", " + sdf.format(expirationDate);
    }
}

