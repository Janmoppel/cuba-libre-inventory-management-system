package sample.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

public class InventoryTool {
    private List<Item> inventory;

    public InventoryTool() {
        inventory = new ArrayList<>();
    }

    public void add(String title, String quantity, String expirationDate) throws Exception {
        boolean isItemFound = false;

            Date inoutDate = new SimpleDateFormat("yyyy-MM-dd").parse(expirationDate);
            for (Item item : inventory) {
                if (item.getTitle().equals(title) && item.getExpirationDate().equals(inoutDate)) {
                    int quantityInt = Integer.parseInt(quantity);
                    item.setQuantity(String.valueOf(item.getQuantity() + quantityInt));
                    isItemFound = true;
                }
            }
            if (!isItemFound) {
                inventory.add(new Item(title, quantity, expirationDate));
            }
    }

    public void deleteExpired() {
        ListIterator<Item> iter = inventory.listIterator();
        while (iter.hasNext()) {
            if (iter.next().getExpirationDate().before(new Date())) {
                iter.remove();
            }
        }
    }

    public void delete(String title) {
        ListIterator<Item> iter = inventory.listIterator();
        while (iter.hasNext()) {
            if (iter.next().getTitle().equals(title)) {
                iter.remove();
            }
        }
    }

    public String inventoryToString() {
        StringBuilder result = new StringBuilder();
        for (Item item : inventory) {
            result.append(item.toString() + "\n");
        }
        return result.toString();
    }

    public String inventoryToStringExpired() {
        StringBuilder result = new StringBuilder();
        for (Item item : inventory) {
            String str = item.toString();
            if (item.getExpirationDate().before(new Date())) {
                result.append("[expired] " + str + "\n");
            } else {
                result.append(str + "\n");
            }
        }
        return result.toString();
    }

    public List<Item> getInventory() {
        return inventory;
    }
}
