package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import sample.model.InventoryTool;

public class Controller {

    @FXML
    private TextArea inventoryTextArea;

    @FXML
    private Pane expiredPane;

    @FXML
    private TextArea inventoryExpiredTextArea;

    @FXML
    private Pane controllPane;

    @FXML
    private Pane mainPane;

    @FXML
    private Pane deletePane;

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField quantityTextField;

    @FXML
    private TextField nameForDeleteTextField;

    @FXML
    private TextField expirationDateTextField;
    private InventoryTool inventoryTool;

    @FXML
    private Label addedItemLbl;

    @FXML
    private Label errorLbl;

    @FXML
    private TextArea itemsTextArea;


    @FXML
    void onAddButtonClick() {
        mainPane.setVisible(true);
        itemsTextArea.setVisible(false);
        deletePane.setVisible(false);
        expiredPane.setVisible(false);
        addedItemLbl.setVisible(false);
        errorLbl.setVisible(false);
        addedItemLbl.setVisible(false);
    }

    @FXML
    void onInventoryIconClick() {
        controllPane.setVisible(true);
    }

    @FXML
    void onDeleteExpiredButtonClick() {
        inventoryTool.deleteExpired();
    }

    @FXML
    void onDeleteButtonClick() {
        mainPane.setVisible(false);
        itemsTextArea.setVisible(false);
        expiredPane.setVisible(false);
        deletePane.setVisible(true);
        errorLbl.setVisible(false);
        addedItemLbl.setVisible(false);
        showInventory();
    }

    @FXML
    void onDeleteConfirmButtonClick() {
        inventoryTextArea.clear();
        String title = nameForDeleteTextField.getText();
        inventoryTool.delete(title);
        showInventory();
    }

    @FXML
    void onSaveButtonClick() {
        mainPane.setVisible(false);
        deletePane.setVisible(false);
        expiredPane.setVisible(false);
        addedItemLbl.setVisible(false);
        itemsTextArea.setVisible(false);
        errorLbl.setVisible(false);
        addedItemLbl.setVisible(false);
        String title = nameTextField.getText();
        String quantity = quantityTextField.getText();
        String expirationDate = expirationDateTextField.getText();
        try {
            inventoryTool.add(title, quantity, expirationDate);
            addedItemLbl.setVisible(true);
            itemsTextArea.setVisible(true);
            itemsTextArea.setText(inventoryTool.inventoryToString());
        } catch (Exception e) {
            errorLbl.setText(e.getMessage());
            errorLbl.setVisible(true);
            e.printStackTrace();
        }
    }

    @FXML
    void onExpiredButtonClick() {
        mainPane.setVisible(false);
        deletePane.setVisible(false);
        itemsTextArea.setVisible(false);
        expiredPane.setVisible(true);
        errorLbl.setVisible(false);
        addedItemLbl.setVisible(false);
        showExpiredInventory();
    }

    @FXML
    private void initialize() {
        inventoryTool = new InventoryTool();
    }

    private void showInventory() {
        inventoryTextArea.setText(inventoryTool.inventoryToString());
    }

    private void showExpiredInventory() {
        inventoryExpiredTextArea.setText(inventoryTool.inventoryToStringExpired());
    }
}
