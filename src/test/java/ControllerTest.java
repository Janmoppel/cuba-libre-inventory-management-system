import org.junit.Assert;
import org.junit.Test;
import sample.model.InventoryTool;
import sample.model.Item;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.mockito.Mockito.*;

public class ControllerTest {

    @Test
    public void addItemWithCorrectParameters() throws Exception {
        InventoryTool tool = new InventoryTool();
        tool.add("Rum", "1", "2025-01-01");
        Assert.assertEquals("Rum, 1, 2025-01-01\n", tool.inventoryToString());
    }

    @Test
    public void addItemThatAlreadyExists() throws Exception {
        InventoryTool tool = new InventoryTool();
        tool.add("Rum", "1", "2025-01-01");
        tool.add("Rum", "5", "2025-01-01");
        Assert.assertEquals("Rum, 6, 2025-01-01\n", tool.inventoryToString());
    }

    @Test
    public void addItemWithWrongParameters()  {
        InventoryTool tool = new InventoryTool();
        try {
            tool.add("Rum", "foo", "2025-01-01");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals("", tool.inventoryToString());
    }

    @Test
    public void deleteItemThatExistsInList() throws Exception {
        InventoryTool tool = new InventoryTool();
        tool.add("Rum", "1", "2025-01-01");
        tool.delete("Rum");
        Assert.assertEquals("", tool.inventoryToString());
    }

    @Test
    public void showExpiredItems() throws Exception {
        InventoryTool tool = new InventoryTool();
        tool.add("Rum", "1", "2025-01-01");
        Item mockItem = mock(Item.class);
        Date expiredDate = new SimpleDateFormat("yyyy-MM-dd").parse("2018-01-01");
        when(mockItem.getExpirationDate()).thenReturn(expiredDate);
        tool.getInventory().add(mockItem);
        when(mockItem.toString()).thenReturn("Whiskey, 10, 2018-01-01");
        Assert.assertEquals("Rum, 1, 2025-01-01\n[expired] Whiskey, 10, 2018-01-01\n", tool.inventoryToStringExpired());
    }

    @Test
    public void deleteExpiredItems() throws Exception {
        InventoryTool tool = new InventoryTool();
        tool.add("Rum", "1", "2025-01-01");
        Item mockItem1 = mock(Item.class);
        Date expiredDate = new SimpleDateFormat("yyyy-MM-dd").parse("2018-01-01");
        when(mockItem1.getExpirationDate()).thenReturn(expiredDate);
        Item mockItem2 = mock(Item.class);
        when(mockItem2.getExpirationDate()).thenReturn(expiredDate);
        tool.getInventory().add(mockItem1);
        tool.getInventory().add(mockItem2);
        tool.deleteExpired();
        Assert.assertEquals("Rum, 1, 2025-01-01\n", tool.inventoryToStringExpired());
    }
}
