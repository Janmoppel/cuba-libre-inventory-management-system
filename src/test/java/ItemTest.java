import org.junit.Assert;
import org.junit.Test;
import sample.model.Item;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import static junit.framework.TestCase.fail;

public class ItemTest {

    @Test
    public void productShortTitleTest() {
        Item itm = new Item();
        itm.setTitle("Cheese");
        Assert.assertEquals("Cheese", itm.getTitle());
    }

    @Test
    public void longNameTest() {
        Item itm = new Item();
        itm.setTitle("Very Long Name that has much more than 50 chars in it, about 69 chars");
        Assert.assertEquals("Very Long Name that has much more than 50 chars in", itm.getTitle());
    }

    @Test
    public void quantityNormal() {
        Item itm = new Item();
        try {
            itm.setQuantity("5");
            Assert.assertEquals(5, itm.getQuantity());
        }catch (Exception e) {
            fail("Unexpected exception");
        }
    }

    @Test
    public void negativeQuantity() {
        Item itm = new Item();
        try{
            itm.setQuantity("-1");
            fail("Quantity has to be positive");
        }catch(Exception e){
            Assert.assertEquals("Quantity has to be positive", e.getMessage());
        }
    }

    @Test
    public void stringInQuantity() {
        Item itm = new Item();
        try {
            itm.setQuantity("abc");
            fail("Quantity has to be integer");
        }catch (Exception e) {
            Assert.assertEquals("Quantity has to be integer", e.getMessage());
        }
    }

    @Test
    public void correctExpirationDate() {
        Item itm = new Item();
        try {
            itm.setExpirationDate("2021-04-02");
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Assert.assertEquals("2021-04-02", formatter.format(itm.getExpirationDate()));
        }catch (Exception e) {
            fail("Unexpected exception");
        }
    }

    @Test
    public void expirationDateSetInPast() {
        Item itm = new Item();
        try {
            itm.setExpirationDate("2017-01-01");
            fail("Expiration date cannot be set for past dates");
        }catch (Exception e) {
            Assert.assertEquals("Expiration date cannot be set for past dates", e.getMessage());
        }
    }

    @Test
    public void expirationDateInWrongFormat() {
        Item itm = new Item();
        try {
            itm.setExpirationDate("01/07/2021");
            fail("Wrong format for expiration date");
        }catch (Exception e) {
            Assert.assertEquals("Wrong format for expiration date", e.getMessage());
        }
    }
}
