# Cuba Libre Inventory Management System


Authors: Vladyslav Kupriienko, Stanislav Deviatykh, Harald Astok, Jan Moppel.

Requirement for development: [*Adding new item to the inventory*](https://bitbucket.org/Janmoppel/cuba-libre-inventory-management-system/wiki/Adding%20new%20item%20to%20the%20inventory)

TDD cycles descriptions:

1. [First cycle](https://bitbucket.org/Janmoppel/cuba-libre-inventory-management-system/wiki/I%20TDD%20cycle)
1. [Second cycle](https://bitbucket.org/Janmoppel/cuba-libre-inventory-management-system/wiki/II%20TDD%20cycle)
1. [Third cycle](https://bitbucket.org/Janmoppel/cuba-libre-inventory-management-system/wiki/III%20TDD%20cycle)


## Installation and usage

1. Download and install the latest stable version of Java SE Runtime Environment (JRE): https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html
1. Download and install git: https://git-scm.com/downloads
1. Download (clone) the repository. Instructions on how to do it: https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html
1. Run the ```/out/artifacts/cuba_libre_inventory_management_system_jar/cuba-libre-inventory-management-system.jar``` file